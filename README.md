# deployka

## Description

deployka is a Bash script to keep your project directory in sync with some
environment.

## Dependencies

- `bash >= 4.3`
- `rsync`
- `inotifywait`

## Usage

Let us imagine you have some web-scripts project on your local host and there
is some dev host to test your solution.

- Put [deployka](deployka) somewhere in your PATH (`~/.local/bin/` may be a
  reasonable choice).
- Put [.deployka.conf](./.deployka.conf) into the project directory and edit to
  match your needs.
- Run `deployka` in a separate terminal window or tab.

If the config and permissions is OK, your dev environment will being synced
with the local project. Sync happens on file changes depends on pointed events.
All remote redundant files will be deleted.

## Local config

A local config file `deployka.conf`, see options in the [example
file](./deployka.conf). Synax is Bash variables.
